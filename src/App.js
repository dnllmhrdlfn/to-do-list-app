import {useState} from 'react';
import AddTaskForm from './components/AddTaskForm.jsx';
import UpdateTaskForm from './components/UpdateTaskForm.jsx';
import ToDo from './components/ToDo.jsx';
import ConfirmationModal from './components/ConfirmationModal.jsx';


import 'bootstrap/dist/css/bootstrap.min.css';

import './App.css';


function App() {

  // Tasks (ToDo List) State
  const [toDo, setToDo] = useState([]);

  // Temp State
  const [newTask, setNewTask] = useState('');
  const [updateData, setUpdateData] = useState('');
  const [showConfirmation, setShowConfirmation] = useState(false);


  // Add Task
  const addTask = () => {
    if(newTask) {
      let num = toDo.length + 1;
      let newEntry = { id: num, title: newTask, status: false }
      setToDo([...toDo, newEntry])
      setNewTask('');
    }
  }

  // Delete Task
  const deleteTask = (id) => {
    let newTask = toDo.filter(task => task.id !== id)
    setToDo (newTask);
  }

  // Mark task as done or completed
  const markDone = (id) => {
    let newTask = toDo.map(task => {
      if (task.id === id ){
        return ({...task, status: !task.status })
      }
      return task;
  
    })
    setToDo(newTask);
  }
  
  // Cancel update
  const cancelUpdate = () => {
    setUpdateData('');
  }
  
  //Change task for update
  const changeTask = (e) => {
    let newEntry = {
      id: updateData.id,
      title: e.target.value,
      status: updateData.status ? true : false
    }
    setUpdateData(newEntry);
  }

  //Update task
  const updateTask = () => {
    let filterRecords = [...toDo].filter(task => task.id !== updateData.id);
    let updatedObject = [...filterRecords, updateData]
    setToDo(updatedObject);
    setUpdateData('');
  }
  
  return (
    <div className="container App">
    <br /><br />
    <h2>Get things done today!</h2>
    <br /><br />

    {updateData && updateData ? (
      <UpdateTaskForm 
        updateData={updateData}
        changeTask = {changeTask}
        updateTask = {updateTask}
        cancelUpdate = {cancelUpdate}
      />


    ) : (
      <AddTaskForm 
        newTask = {newTask}
        setNewTask ={setNewTask}
        addTask ={addTask}
      />

    )}
    
    {/* Display ToDos */}

    {toDo && toDo.length ? '':'Tasks show up here.'}

    <ToDo 
      toDo = {toDo}
      markDone = {markDone}
      setUpdateData ={setUpdateData}
      deleteTask = {setShowConfirmation}
    />

    <ConfirmationModal
      isOpen={showConfirmation !== null}
      onCancel={() => setShowConfirmation(null)}
      onConfirm={() => {
        deleteTask(showConfirmation);
        setShowConfirmation(null);
      }}
    />

    </div>
    
    

  );
};
export default App;
