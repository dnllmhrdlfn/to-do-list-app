const deleteButton = document.querySelector('.delete-button');
const modal = document.querySelector('.confirmation-modal');
const cancelButton = document.querySelector('.cancel-button');
const confirmButton = document.querySelector('.confirm-button');

deleteButton.addEventListener('click', () => {
  modal.classList.add('show');
});

cancelButton.addEventListener('click', () => {
  modal.classList.remove('show');
});

confirmButton.addEventListener('click', () => {
  // Perform the delete operation here
  // ...
  modal.classList.remove('show');
});
